﻿using UnityEngine;

public class Shield : MonoBehaviour
{

    public GameObject shieldIcon;

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Shield"))
        {
            shieldIcon.SetActive(true);
            Destroy(GetComponent<CanBeForced>());
        }
        else if (Input.GetButtonUp("Shield"))
        {
            shieldIcon.SetActive(false);
            gameObject.AddComponent<CanBeForced>();
        }
    }
}
