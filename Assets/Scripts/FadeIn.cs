﻿using UnityEngine;

public class FadeIn : MonoBehaviour
{

    float alphaFadeValue = 1;
    public float duration = 5;
    public Texture blackTexture;

    // Update is called once per frame
    void OnGUI()
    {
        alphaFadeValue -= Mathf.Clamp01(Time.deltaTime / duration);
        GUI.color = new Color(0, 0, 0, alphaFadeValue);
        GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), blackTexture);
        if (alphaFadeValue <= 0)
        {
            Destroy(this);
        }
    }
}
