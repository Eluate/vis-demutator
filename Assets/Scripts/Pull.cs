﻿using UnityEngine;

public class Pull : Orb
{
    public override Vector3 CalculateForce(Collider other)
    {
        Vector3 distanceVector = other.transform.position - transform.position;
        float distance = distanceVector.sqrMagnitude / 8;
        if (distance < 1f)
        {
            distance = 1;
        }
        return (distanceVector * strength) / distance;
    }
}
