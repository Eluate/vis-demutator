﻿using UnityEngine;

public abstract class Orb : MonoBehaviour
{
    public float strength = 16f;
    public bool isActive = true;

    void OnTriggerStay(Collider other)
    {
        if (!isActive)
        {
            return;
        }

        var otherBody = other.GetComponent<Rigidbody>();
        var otherCanBeForced = other.GetComponent<CanBeForced>();
        if (otherBody != null && otherCanBeForced != null)
        {
            otherBody.AddForce(CalculateForce(other), ForceMode.Acceleration);
        }
    }

    public abstract Vector3 CalculateForce(Collider other);
}
