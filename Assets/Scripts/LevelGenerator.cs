﻿using UnityEngine;

public class LevelGenerator : MonoBehaviour
{
    // This class was orignally used to generate the levels in order to prevent being slowed down by level construction during the development process

    [SerializeField] private int roomAmount;
    [SerializeField] private GameObject roomObject;
    [SerializeField] private GameObject[] connectorObjects;
    [SerializeField] private int roomWidth;
    [SerializeField] private int connectorWidth;
    private Vector3 position;
    private int roomCount;
    private Vector3[] roomPositions;
    private Vector3[] connectorPositions;
    private Vector3[] connectorDirections;


    // Use this for initialization
    void Start()
    {
        roomPositions = new Vector3[roomAmount];
        connectorPositions = new Vector3[roomAmount];
        connectorDirections = new Vector3[roomAmount];
        while (roomCount < roomAmount)
        {
            int rand = Random.Range(0, 4);
            switch (rand)
            {
                case 0:
                    LogPosition(Vector3.up);
                    break;
                case 1:
                    LogPosition(Vector3.left);
                    break;
                case 2:
                    LogPosition(Vector3.forward);
                    break;
                case 3:
                    LogPosition(Vector3.right);
                    break;
                case 4:
                    LogPosition(Vector3.down);
                    break;
                case 5:
                    LogPosition(Vector3.back);
                    break;
            }
            roomCount++;
        }
        CreateRooms();
        CreateConnectors();
        OverlapCheck();
    }

    private void LogPosition(Vector3 direction)
    {
        position += direction * roomWidth;
        position -= direction * 2;
        connectorPositions[roomCount] = position;
        connectorDirections[roomCount] = direction;
        position += direction * 2;
        position += direction * connectorWidth;
        roomPositions[roomCount] = position;
    }

    private void CreateRooms()
    {
        for (var i = 0; i < roomAmount; i++)
        {
            bool dupeFound = false;
            for (var n = 0; n < roomAmount; n++)
            {
                if (i == n)
                {
                    continue;
                }
                if (roomPositions[i] == roomPositions[n] && i > n)
                {
                    dupeFound = true;
                    n = roomAmount;
                }
            }
            if (!dupeFound)
            {
                Instantiate(roomObject, roomPositions[i], Quaternion.identity);
            }
            if (i > 0)
            {
                RaycastHit hitInfo;
                if (Physics.Linecast(roomPositions[i - 1], roomPositions[i], out hitInfo))
                {
                    DestroyImmediate(hitInfo.transform.gameObject);
                }
                if (Physics.Linecast(roomPositions[i - 1], roomPositions[i], out hitInfo))
                {
                    DestroyImmediate(hitInfo.transform.gameObject);
                }
            }
        }
    }

    private void CreateConnectors()
    {
        for (var i = 0; i < roomAmount; i++)
        {
            GameObject connectorObject;
            if (connectorDirections[i] == Vector3.forward || connectorDirections[i] == Vector3.back)
            {
                connectorObject = connectorObjects[1];
            }
            else if (connectorDirections[i] == Vector3.left || connectorDirections[i] == Vector3.right)
            {
                connectorObject = connectorObjects[0];
            }
            else
            {
                connectorObject = connectorObjects[2];
            }
            Instantiate(connectorObject, connectorPositions[i] - new Vector3(0, 3, 0), Quaternion.identity);
        }
    }

    private void OverlapCheck()
    {
        GameObject[] objectsInScene = GameObject.FindGameObjectsWithTag("Untagged");
        for (int i = 0; i < objectsInScene.Length; i++)
        {
            if (objectsInScene[i].GetComponent<MeshFilter>())
            {
                for (int n = 0; n < objectsInScene.Length; n++)
                {
                    if (i != n && objectsInScene[n].GetComponent<MeshFilter>())
                    {
                        if (objectsInScene[i].transform.position == objectsInScene[n].transform.position)
                        {
                            DestroyImmediate(objectsInScene[n]);
                        }
                    }
                }
            }
        }

    }
}
