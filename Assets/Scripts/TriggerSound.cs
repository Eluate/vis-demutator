﻿using UnityEngine;

public class TriggerSound : MonoBehaviour
{

    public bool playerOnly = true;
    public bool onlyOnce = true;
    public AudioSource audioToPlay;

    void OnTriggerEnter(Collider other)
    {
        if (playerOnly)
        {
            if (other.gameObject.CompareTag("Player"))
            {
                audioToPlay.Play();
            }
        }
        else
        {
            audioToPlay.Play();
        }
        if (onlyOnce)
        {
            Destroy(this);
        }
    }
}
