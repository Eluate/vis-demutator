﻿using UnityEngine;

public class LookAt : MonoBehaviour
{

    public Transform target;

    // Update is called once per frame
    void Update()
    {
        gameObject.transform.LookAt(target);
    }
}
