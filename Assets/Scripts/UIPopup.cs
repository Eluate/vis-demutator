﻿using UnityEngine;

public class UIPopup : MonoBehaviour
{

    public GameObject objectToEnable;
    public GameObject objectGroup;

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            RectTransform[] rectTransformGroup = objectGroup.GetComponentsInChildren<RectTransform>();
            int objectGroupLength = rectTransformGroup.Length;
            for (int i = 0; i < objectGroupLength; i++)
            {
                rectTransformGroup[i].gameObject.SetActive(false);
            }
            objectGroup.SetActive(true);
            if (objectToEnable)
            {
                objectToEnable.SetActive(true);
            }
        }
    }
}
