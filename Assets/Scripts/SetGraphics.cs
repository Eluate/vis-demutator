﻿using UnityEngine;

public class SetGraphics : MonoBehaviour
{

    public void SetSetting(int qualityIndex)
    {
        QualitySettings.SetQualityLevel(qualityIndex);
    }

}
