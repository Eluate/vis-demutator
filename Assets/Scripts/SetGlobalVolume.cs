﻿using UnityEngine;
using UnityEngine.UI;

public class SetGlobalVolume : MonoBehaviour
{
    public Slider slider;
    public void SetVolume()
    {
        AudioListener.volume = slider.value;
    }
}
