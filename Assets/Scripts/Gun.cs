﻿using UnityEngine;
using UnityEngine.UI;

public class Gun : MonoBehaviour
{

    [SerializeField] private GameObject pullPrefab;
    [SerializeField] private GameObject pushPrefab;
    [SerializeField] private bool infiniteAmmo;
    [SerializeField] private int pushAmmo;
    [SerializeField] private int pullAmmo;
    [SerializeField] private Text pushText;
    [SerializeField] private Text pullText;
    [SerializeField] private AudioClip pushSound;
    [SerializeField] private AudioClip pullSound;

    private GameObject pullObject;
    private GameObject pushObject;
    private GameObject pullInit = null;
    private GameObject pushInit = null;
    private Vector3 direction;
    private Camera playCamera;
    private AudioSource audioSource;

    // Use this for initialization
    void Start()
    {
        playCamera = Camera.main;
        audioSource = gameObject.AddComponent<AudioSource>();
        if (pushText && pullText)
        {
            UpdateUI();
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Fire1") && pullAmmo > 0)
        {
            audioSource.clip = pullSound;
            audioSource.Play();
            if (pullObject)
            {
                Destroy(pullObject);
            }
            if (!infiniteAmmo) pullAmmo--;
            pullInit = InstantiateOrb(pullPrefab);
        }
        else if (Input.GetButtonUp("Fire1"))
        {
            if (pullInit)
            {
                pullInit.GetComponent<Pull>().isActive = true;
                pullObject = pullInit;
                pullInit = null;
            }
        }
        else if(Input.GetButtonDown("Fire2") && pushAmmo > 0)
        {
            audioSource.clip = pushSound;
            audioSource.Play();
            if (pushObject)
            {
                Destroy(pushObject);
            }
            if (!infiniteAmmo) pushAmmo--;
            pushInit = InstantiateOrb(pushPrefab);
        }
        else if(Input.GetButtonUp("Fire2"))
        {
            if (pushInit)
            {
                pushInit.GetComponent<Push>().isActive = true;
                pushObject = pushInit; 
                pushInit = null;
            }
        }

        TransportOrb(pushInit);
        TransportOrb(pullInit);
    }

    GameObject InstantiateOrb(GameObject orbPrefab)
    {
        var prefab = Instantiate(orbPrefab, transform.position, transform.rotation);
        prefab.GetComponentInChildren<LookAt>().target = playCamera.transform;
        prefab.GetComponent<Orb>().isActive = false;
        direction = playCamera.transform.forward;

        UpdateUI();
        return prefab;
    }

    void TransportOrb(GameObject orb)
    {
        if (orb != null)
        {
            orb.transform.position = Vector3.Lerp(orb.transform.position, orb.transform.position + direction, Time.deltaTime * 10);
        }
    }

    void UpdateUI()
    {
        pushText.text = pushAmmo.ToString();
        pullText.text = pullAmmo.ToString();
    }

}
