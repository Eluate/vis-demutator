﻿using UnityEngine;

public class ObjectToggle : MonoBehaviour
{

    public GameObject objectToToggle;

    // Call this for toggle
    public void OnPress()
    {
        if (objectToToggle.activeInHierarchy)
        {
            objectToToggle.SetActive(false);
        }
        else
        {
            objectToToggle.SetActive(true);
        }
    }
}
