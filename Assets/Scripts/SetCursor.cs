﻿using UnityEngine;

public class SetCursor : MonoBehaviour
{

    void OnLevelWasLoaded(int level)
    {
        if (level != 0)
        {
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
        }
        else
        {
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
        }
    }

}
