﻿using UnityEngine;

public class DoNotDestroy : MonoBehaviour
{

	void Start()
	{
		DoNotDestroy[] destroyComponents = FindObjectsOfType(typeof(DoNotDestroy)) as DoNotDestroy[];
		for (int i = 0; i < destroyComponents.Length; i++)
		{
			if (GetInstanceID() != destroyComponents[i].GetInstanceID() && destroyComponents[i].gameObject.name == this.gameObject.name)
			{
				Destroy(gameObject);
			}
		}
		DontDestroyOnLoad(gameObject);
	}

}
