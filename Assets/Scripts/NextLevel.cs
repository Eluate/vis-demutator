﻿using System.Collections;
using UnityEngine;

public class NextLevel : MonoBehaviour
{
    // Change level variables
    public float delay = 1f;
    public bool loadNextLevel;
    public bool loadThisLevel;
    public int loadSpecificLevel;
    private bool hasTriggered = false;
    // Fade out variables
    float alphaFadeValue = 0f;
    public float duration = 1f;
    public Texture blackTexture;

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            StartCoroutine(ChangeLevels());
            hasTriggered = true;
        }
    }

    IEnumerator ChangeLevels()
    {
        yield return new WaitForSeconds(delay);
        if (loadNextLevel)
        {
            Application.LoadLevel(Application.loadedLevel + 1);
        }
        else if (loadThisLevel)
        {
            Application.LoadLevel(Application.loadedLevel);
        }
        else
        {
            Application.LoadLevel(loadSpecificLevel);
        }
    }

    void OnGUI()
    {
        if (!hasTriggered)
        {
            return;
        }
        alphaFadeValue += Mathf.Clamp01(Time.deltaTime / duration);

        GUI.color = new Color(0, 0, 0, alphaFadeValue);
        GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), blackTexture);
    }

    // To be called from UI
    public void LoadLevel()
    {
        hasTriggered = true;
        if (!loadNextLevel)
        {
            loadSpecificLevel = PlayerPrefs.GetInt("level", -1);
            if (loadSpecificLevel == -1)
            {
                loadNextLevel = true;
            }
        }
        StartCoroutine(ChangeLevels());
    }

    public void ToggleNext(bool toggle)
    {
        loadNextLevel = toggle;
    }
}
