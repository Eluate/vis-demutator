﻿using UnityEngine;

public class AddForce : MonoBehaviour
{

	[SerializeField] private int force;
	[SerializeField] private Vector3 direction;

	void Start()
	{
		GetComponent<Rigidbody>().velocity = force * direction.normalized;
	}
}
