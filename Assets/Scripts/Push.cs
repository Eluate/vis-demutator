﻿using UnityEngine;

public class Push : Orb
{
    public override Vector3 CalculateForce(Collider other)
    {
        Vector3 distanceVector = other.transform.position - transform.position;
        float distance = distanceVector.sqrMagnitude / 8;
        return distanceVector * strength / distance;
    }
}
