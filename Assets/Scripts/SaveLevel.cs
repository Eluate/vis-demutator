﻿using UnityEngine;

public class SaveLevel : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {
        if (Application.loadedLevel > 2)
        {
            PlayerPrefs.SetInt("level", Application.loadedLevel);
            PlayerPrefs.Save();
        }
    }
}
